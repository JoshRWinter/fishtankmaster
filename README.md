Master Server for fishtank game

This software allows anyone to start the "normal" fishtank server, and have it appear on the in-game server browser.  
The master server provides 2 functions:

1. Provide a server listing of all registered servers
2. Allow any server to register itself to this software
